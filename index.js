const express = require('express');
const app = express();
const PORT = 7000;

// Middleware
app.use(express.json());

// Routes
app.get('/', (req, res) => {
  res.send('<p style="color: #997bff;">Hi, I have created multiple environments and this docker image is with snyk container test done and downloaded from my personal docker hub and deployed in local kubernetes using kustomize!</p>');
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
